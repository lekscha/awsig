# Areawise significance tests for recurrence network analysis

Project: Areawise significance tests for recurrence network analysis
by Jaqueline Lekscha and Reik V. Donner - 
code for analyses performed in corresponding paper (submitted to Proceedings of the Royal Society A)

