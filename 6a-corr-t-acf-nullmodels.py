# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 6a-corr-t-acf-nullmodels.py: python script to plot autocorrelation functions (time domain) for a given nullmodel

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc
import matplotlib.pyplot as pyplot
from matplotlib import rc 
import matplotlib.font_manager

rc('text',usetex=True)


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# set parameters
dim = 3
dW = 1
rnm = 0
NG = 1 # choose realisation of which acf to plot
N = 2000

# choose window width
Ws = np.arange(100,301)

Nw = int((N-max(Ws)))
maxlag = Nw/4

# do plot
pyplot.figure(1,figsize = (8,5))
pyplot.plot(np.arange(maxlag),1./np.exp(1.)*np.ones(maxlag),'--', color='black', linewidth=1.0)
pyplot.plot(np.arange(maxlag),np.zeros(maxlag),'-', color='black', linewidth=1.0)

for W in Ws:

    time = np.arange(max(Ws),N+1)

    # load transitivity data
    res = np.loadtxt("./results/AR1-%s-wrna-dim%s-W%s-dW%s.txt" % (NG, dim, W, dW)) # or load any other data here
    T = res[:,rnm]

    # calculate autocorrelation
    lags, acf = awsfunc.autocorrelation(time, T, maxlag)

    # plot autocorrelation
    pyplot.plot(lags,acf,'-', color=(1.2-W/300.,0.1,0.2), linewidth=1.0)


pyplot.ylabel('$\mathrm{autocorrelation}$', size=21)
pyplot.xlabel('$\mathrm{lag}$', size=21)
pyplot.xticks(size=21)
pyplot.yticks(size=21)
pyplot.xlim(0,maxlag)
pyplot.tight_layout()
pyplot.savefig('./plots/acf-AR1-t-dim%s-NG%s.eps' % (dim,NG), format='eps', dpi = 200)
pyplot.show()





