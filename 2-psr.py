# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 2-psr.py: python script for phase space reconstruction of given time series using uniform time delay embedding

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import matplotlib.pyplot as pyplot
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# set parameters
dim = 3
N = 2000
time = np.arange(1,N+1)
maxlag = N/10

# calculate and plot lagged autocorrelation of chosen time series (get first zero as delay and save them in corresponding file, then comment this part and uncomment the next)
count = 1
data = np.loadtxt("./data/AR1-%s.txt" % (count)) # or load any other time series here

lags, acf = awsfunc.autocorrelation(time, data, maxlag)

pyplot.figure(count,figsize = (8,5))
pyplot.plot(lags, np.zeros(maxlag), color='black')
pyplot.plot(lags, acf,'o-', color='lightcoral')
pyplot.show()

"""

# load taus
taus = np.loadtxt("./data/AR1-delays.txt")
taus = taus.astype(int)

# phase space reconstruction using uniform time delay embedding
count = 1

var = np.loadtxt("./data/AR1-%s.txt" % (count)) # or load any other time series here
emb_ts = np.zeros((N-(dim-1)*taus[count-1], dim+1))
emb_ts[:,0] = time[(dim-1)*taus[count-1]:]
emb_ts[:,1:] = awsfunc.tde(time,var,dim,taus[count-1])
awsfunc.stf2d(emb_ts, './data/AR1-%s-tde%s-dim%s.txt' % (count, taus[count-1], dim))

"""
