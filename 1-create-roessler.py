# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 1-create-roessler.py: python script that creates the time series for the non-stationary Rössler system

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from matplotlib import rc 
import matplotlib.font_manager
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# set plotting properties
-------------------------------------------------------------------------------------------------------------- """
rc('text',usetex=True)
rc('xtick', labelsize=21) 
rc('ytick', labelsize=21)


""" --------------------------------------------------------------------------------------------------------------
# local subroutines
-------------------------------------------------------------------------------------------------------------- """
def roessler_system(vec,t):
    x, y, z = vec
    return[-y-z, x+a*y, b+z*(x-c)]


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# create time array 
time = np.arange(1,2001)
N = len(time)

# Roessler system - non-stationary
var, time = awsfunc.roessler(N, [0.5,0.0,0.0,0.02])

# save data
f1 = open('./input/roessler.txt', 'w')
for counter in range(N): 
    f1.write(str(var[counter]))
    f1.write("\n")
f1.close()
f2 = open('./input/bs.txt', 'w')
for counter in range(N): 
    f2.write(str(time[counter]))
    f2.write("\n")
f2.close()


# check whether data have zero mean and unit variance 
print np.mean(var)
print np.var(var)


# plot the time series and corresponding bifurcation diagram of stationary Roessler system
# set parameters
a = 0.2
c = 5.7
bs = np.arange(0.01,0.7,0.002)
n = len(bs)

last = 2500

ini = [0.5,0.,0.]

t = np.arange(0,1000,0.1)

# figure starts here
fig, (ax1, ax2) = plt.subplots(2,1,figsize=(10,8),sharex=True)

ax1.plot(time, var, color='black')

ax1.set_ylabel('$x$',size=21)
ax1.set_xlim(0.01,0.65)
ax1.set_yticks([-1,0,1,2])

for count in range(n):
    b = bs[count]
    sol = odeint(roessler_system, ini, t)
    xx, yy, zz = sol.T

    #get values of x in last iterations where y goes from positive to negative
    xrec = []
    for i in range(last):
        if yy[-1-i]<0 and yy[-2-i]>=0:
            xrec.append(xx[-1-i])

    lx = len(xrec)

    ax2.plot(bs[count]*np.ones(lx),xrec,',k',ms=2.0)

ax2.axvline(x=0.0367,linewidth=1.0,color='k')
ax2.axvline(x=0.0931,linewidth=1.0,color='k')
ax2.axvline(x=0.259,linewidth=1.0,color='k')
ax2.axvline(x=0.367,linewidth=1.0,color='k')
ax2.axvline(x=0.5017,linewidth=1.0,color='k')
ax2.axvline(x=0.5134,linewidth=1.0,color='k')

ax2.set_xlabel('$b$',size=21)
ax2.set_ylabel('$x$',size=21)
ax2.set_xlim(0.01,0.65)
ax2.set_yticks([-3,-5,-7,-9])
plt.tight_layout()
plt.savefig('./plots/ts-bifdiag-bx-T5.eps', format='eps', dpi = 500)
plt.show()









