# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 7b-fit-lines-Ws-all-back.py: python script to peform linear fit of decorrelation time in the window width domain and to plot result

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import matplotlib.pyplot as pyplot
from matplotlib import rc 
import matplotlib.font_manager

rc('text',usetex=True)


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# set parameters
dim = 3
dW = 1
rnm = 0
NG = 100
N = 2000

# choose window widths
Ws = np.arange(100,301)
Nw = len(Ws)

"""load results for window width domain - GWN"""
res = np.zeros((Nw,NG))
for count in range(1,NG+1):
    data = np.loadtxt('./eval/GWN-%s-lagvals2s-Ws-2e-dim%s-W300.txt' % (count, dim))
    res[:,count-1] = data[:,2]

lagmean = np.mean(res[63:],axis=1)
lagstd = np.std(res[63:],axis=1)

Nw1 = len(Ws[63:])
Ws1 = Ws[63:]

#do least squares linear regression of the mean
A = np.vstack([Ws1, np.ones(Nw1)]).T
m, n = np.linalg.lstsq(A, lagmean)[0]

print m
print n

# coefficient of determination R**2
ymean = np.mean(lagmean)
yhat = m*Ws1 + n
diffhatsq = (yhat - ymean)**2
diffsq = (lagmean-ymean)**2 
residuals = (lagmean-yhat)**2
Rsquared = 1. - np.sum(residuals)/np.sum(diffsq)

print Rsquared



"""load results for window width domain - AR1"""
res = np.zeros((Nw,NG))
for count in range(1,NG+1):
    data = np.loadtxt('./eval/AR1-%s-lagvals2s-Ws-2e-dim%s-W300.txt' % (count, dim))
    res[:,count-1] = data[:,2]

lagmean2 = np.mean(res[110:],axis=1)
lagstd2 = np.std(res[110:],axis=1)

Nw2 = len(Ws[110:])
Ws2 = Ws[110:]

#do least squares linear regression of the mean
A = np.vstack([Ws2, np.ones(Nw2)]).T
m2, n2 = np.linalg.lstsq(A, lagmean2)[0]

print m2
print n2

# coefficient of determination R**2
ymean = np.mean(lagmean2)
yhat = m2*Ws2 + n2
diffhatsq = (yhat - ymean)**2
diffsq = (lagmean2-ymean)**2 
residuals = (lagmean2-yhat)**2
Rsquared = 1. - np.sum(residuals)/np.sum(diffsq)

print Rsquared



"""load results for window width domain - da trw"""
res = np.zeros((Nw,NG))
for count in range(1,NG+1):
    data = np.loadtxt('./eval/da-%s-lagvals2s-Ws-2e-dim%s-W300.txt' % (count, dim))
    res[:,count-1] = data[:,2]

lagmean3 = np.mean(res[118:],axis=1)
lagstd3 = np.std(res[118:],axis=1)

Nw3 = len(Ws[118:])
Ws3 = Ws[118:]

#do least squares linear regression of the mean
A = np.vstack([Ws3, np.ones(Nw3)]).T
m3, n3 = np.linalg.lstsq(A, lagmean3)[0]

print m3
print n3


# coefficient of determination R**2
ymean = np.mean(lagmean3)
yhat = m3*Ws3 + n3
diffhatsq = (yhat - ymean)**2
diffsq = (lagmean3-ymean)**2 
residuals = (lagmean3-yhat)**2
Rsquared = 1. - np.sum(residuals)/np.sum(diffsq)

print Rsquared




# plot as a function of the window width
pyplot.figure(1,figsize = (8,5))
pyplot.errorbar(Ws1,lagmean,yerr=lagstd,fmt='o', color='lightblue',capsize=2,capthick=1,ms=8.0, linewidth=1.0,zorder=1)
pyplot.errorbar(Ws2,lagmean2,yerr=lagstd2,fmt='o', color='lightgreen',capsize=2,capthick=1,ms=8.0, linewidth=1.0,zorder=2)
pyplot.errorbar(Ws3,lagmean3,yerr=lagstd3,fmt='o', color='lightcoral',capsize=2,capthick=1,ms=8.0, linewidth=1.0,zorder=3)
pyplot.plot(Ws1,m*Ws1+n,'-.', color='darkblue', linewidth=3.0,zorder=4)
pyplot.plot(Ws2,m2*Ws2+n2,'--', color='darkgreen', linewidth=3.0,zorder=5)
pyplot.plot(Ws3,m3*Ws3+n3,'-', color='darkred', linewidth=3.0,zorder=6)
pyplot.ylabel('$\mathrm{decorrelation~length}$', size=21)
pyplot.xlabel('$\mathrm{window~width}$', size=21)
pyplot.xticks(size=21)
pyplot.yticks(size=21)
pyplot.text(160,127,'$(\mathrm{a})$',size=21)
pyplot.tight_layout()
pyplot.savefig('./plots/ross-decorrs2sback-Ws-fit-errorbars-dim3-W300.eps', format='eps', dpi = 200)
pyplot.show()


