# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 3-create-nullmodels.py: python script that creates a specified amount of realisations of the different null models (GWN,AR(1) and data-adaptive routine using iAAFT surrogates)

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# choose number of null model time series to be created and their length
NG = 100
N = 2000

# set parameters for GWN 
mean = 0.
stdev = 1.

# create input time series
for count in range(1,NG+1):
    var = awsfunc.Gnoise(N, mean, stdev)
  
    # save data
    f1 = open('./data/GWN-%s.txt' % count, 'w')
    for counter in range(N): 
        f1.write(str(var[counter]))
        f1.write("\n")
    f1.close()


# set parameters for AR1 process (obtained e.g. by fitting AR1 process to data under study)
start = 0.1897
scale = 0.9427
sig = 0.3337
const = 0.

# create input time series
for count in range(1,NG+1):
    var = awsfunc.ar1_c_nn(N, start, const, scale, sig)
  
    # save data
    f1 = open('./data/AR1-%s.txt' % count, 'w')
    for counter in range(N): 
        f1.write(str(var[counter]))
        f1.write("\n")
    f1.close()


# load original data for creating data-adaptive surrogates
var = np.loadtxt("./input/roessler.txt")
Niter = 20

# create input time series
for count in range(1,NG+1):
    #create iAAFT surrogate time series
    surr = awsfunc.iAAFT(var, Niter)

    # save data
    f1 = open('./data/DA-%s.txt' % count, 'w')
    for counter in range(N): 
        f1.write(str(surr[counter]))
        f1.write("\n")
    f1.close()
















