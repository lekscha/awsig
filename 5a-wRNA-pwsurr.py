# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 5a-wRNA-pwsurr.py: python script to perform windowed recurrence network analysis of a given time series including pointwise random shuffling surrogate test

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# set parameters
dim = 3
Ws = np.arange(100,301)
dW = 1
RR = 0.05
Nsurr = 1000

taus = np.loadtxt("./data/AR1-delays.txt") # or load any other data set here 
taus = taus.astype(int)

count = 1

# windowed recurrence network analysis
for W in Ws:
    data_emb = np.loadtxt("./data/AR1-%s-tde%s-dim%s.txt" % (count, taus[count-1], dim)) # or load any other data set here
    T, L = awsfunc.wrna(data_emb[:,1:], W, dW, RR)
    Nw = len(T)
    TL = np.zeros((Nw,2))
    TL[:,0], TL[:,1] = T, L
    awsfunc.stf2d(TL, './results/AR1-%s-wrna-dim%s-W%s-dW%s.txt' % (count, dim, W, dW))

# random shuffling surrogates for wrna
for W in Ws:
    data_emb = np.loadtxt("./data/AR1-%s-tde%s-dim%s.txt" % (count, taus[count-1], dim)) # or load any other data set here
    T, L = awsfunc.wrna_surr(data_emb[:,1:], W, RR, Nsurr)
    TL = np.zeros((Nsurr,2))
    TL[:,0], TL[:,1] = T, L
    awsfunc.stf2d(TL, './surr/AR1-%s-surr-dim%s-W%s-dW%s.txt' % (count, dim, W, dW))



