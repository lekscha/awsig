# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 6b-corr-t-nullmodels.py: python script to calculate autocorrelation functions (time domain) for a given nullmodel

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# local subroutines
-------------------------------------------------------------------------------------------------------------- """
def getval(ac, lags):

    # get lag at which value of acf falls below 1/e
    valac = ac[0]
    siglag = 0
    while valac > 1./np.exp(1.) and siglag < len(lags)-1:
        valac = ac[siglag+1]
        siglag = siglag + 1         

    return siglag + 1


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# set parameters
dim = 3
dW = 1
rnm = 0
NG = 100
N = 2000

# choose window widths
Ws = np.arange(100,301)
lagvals = np.zeros((len(Ws),NG))
wcount = 0

Nw = int((N-max(Ws)))
maxlag = Nw/2


for W in Ws:

    time = np.arange(max(Ws),N+1)

    for count in range(1,NG+1):
        # load transitivity data
        res = np.loadtxt("./results/AR1-%s-wrna-dim%s-W%s-dW%s.txt" % (count, dim, W, dW)) # or load any other data here
        T = res[:,rnm]

        # calculate autocorrelation
        lags, acf = awsfunc.autocorrelation(time, T, maxlag)

        lagvals[wcount,count-1] = getval(acf, lags)

    wcount = wcount + 1


# save to file
for count in range(1,NG+1):
    Wsig = np.zeros((len(Ws),2))
    Wsig[:,0], Wsig[:,1] = Ws, lagvals[:,count-1]
    awsfunc.stf2d(Wsig, './eval/AR1-%s-lagvals-t-1e-dim%s-W300.txt' % (count, dim))







