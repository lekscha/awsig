# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# awsfunc.py: python file containing the functions that need to be imported to run the following scripts

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
from __future__ import division
import numpy as np
from scipy import stats
from scipy.integrate import odeint
from pyunicorn.timeseries import RecurrenceNetwork
from pyunicorn.timeseries import Surrogates


""" --------------------------------------------------------------------------------------------------------------
# subroutines
-------------------------------------------------------------------------------------------------------------- """
# parameters for Roessler system
d = 0.2
f = 5.7

def autocorrelation(time, var, maxlag):
    #calculate autocorrelation of regularly sampled data lagged up to maxlag
    lags = np.arange(maxlag)
    ac = np.zeros(maxlag)
    for counter in range(maxlag):
        slope, i, ac[counter], p, s = stats.linregress(var[:-(counter+1)],var[counter:-1])
    return [lags, ac]


def tde(time,var,dim,tau):

    #time delay embedding
    N = len(time)
    Nemb = N - (dim-1)*tau
    x_tde = np.zeros((Nemb, dim))

    for dimcounter in range(dim):
        x_tde[:, dimcounter] = var[dimcounter*tau:N-(dim-1-dimcounter)*tau]

    return x_tde


def wrna(emb_ts, W, dW, RR):

    #windowed recurrence network analysis
    N = len(emb_ts[:,0])
    Nwind = int(((N-W)/dW))

    T = np.zeros(Nwind)
    L = np.zeros(Nwind)

    for mu in range(Nwind):
        rn = RecurrenceNetwork(emb_ts[mu*dW:mu*dW+W,:], recurrence_rate=RR, silence_level=2)
        T[mu] = rn.transitivity()
        L[mu] = rn.average_path_length()

    return [T, L]


def wrna_surr(emb_ts, W, RR, Nsurr):

    #surrogates from embedded time series to get RNA significance bounds
    N = len(emb_ts[:,0])

    T = np.zeros(Nsurr)
    L = np.zeros(Nsurr)

    for surr in range(Nsurr):
        indices = np.arange(N)
        np.random.shuffle(indices)
        rand = indices[:W]

        rn = RecurrenceNetwork(emb_ts[rand,:], recurrence_rate=RR, silence_level=2)

        T[surr] = rn.transitivity()
        L[surr] = rn.average_path_length()

    return [T, L]


def iAAFT(var, Niter):

    #create iAAFT surrogate data from (univariate) time series
    origData = np.array([var])
    surr = Surrogates(origData,silence_level=2)
    surr.clear_cache()
    surro = surr.refined_AAFT_surrogates(origData, Niter)
    surrogates = surro[0,:]

    return surrogates



def stf2d(data, name):

    #save data to file named name
    N = len(data[:,0])
    M = len(data[0,:])

    f = open(name, 'w')
    for counterN in range(N): 
        for counterM in range(M):
            f.write(str(data[counterN,counterM]))
            f.write("\t")
        f.write("\n")
    f.close()


def areawise_test(Nt, Ws, Spw, dt, mWW, flW, mWt, flt):
    Nw = len(Ws)
    Saw = np.zeros((Nt,Nw))

    Wmin = min(Ws)
    Wmax = max(Ws)
    dW = (Wmax-Wmin)/(Nw-1)

    kws = mWW/(2.*dt) + flW*(Ws)/(2.*dt)
    kts = mWt/(2.*dt) + flt*(Ws)/(2.*dt)

    kws = kws.astype(int)
    kts = kts.astype(int)

    ktmax = max(kts)
    kwmax = max(kws)
    
    if kwmax == 1:
        Spwpadded = np.zeros((Nt+2*(ktmax-1),Nw+2*(kwmax)))
        Spwpadded[ktmax-1:-(ktmax-1),kwmax:-(kwmax)] = Spw
    else:
        Spwpadded = np.zeros((Nt+2*(ktmax-1),Nw+2*(kwmax-1)))
        Spwpadded[ktmax-1:-(ktmax-1),kwmax-1:-(kwmax-1)] = Spw
    
    for wc in range(Nw):
        for tc in range(Nt):

            if Spw[tc, wc] == 1.:
                Saw[tc,wc] = perform_test(Spwpadded, kts[wc], kws[wc], wc+kwmax-1, tc+ktmax-1)

    return Saw


def perform_test(Spw, kt, kw, wc, tc):

    if np.sum(Spw[tc-kt+1:tc+kt,wc-kw+1:wc+kw]) == (2.*kt-1.)*(2.*kw-1.):
        testpass = 1.
    else:
        testpass = 0.
 
    return testpass


def roessler_system(vec,t):
    x, y, z, e = vec
    return[-y-z, x+d*y, e+z*(x-f), 0.001]

def Gnoise(N, mu, sig):
    var = np.random.normal(mu, sig, N)
    return var

def ar1_c_nn(N, start, const, scale, sig):
    var = np.zeros(N)
    var[0] = start
    for count in range(1,N):
        var[count] = const+scale*var[count-1]+np.random.normal(0.,sig)
    return var


def roessler(N,ini):
    t = np.arange(0.,630.,0.1)
    ts = odeint(roessler_system, ini, t)
    xx, yy, zz, bb = ts.T
    var = (xx[300::3] - np.mean(xx[300::3]))/np.std(xx[300::3])
    return [var, bb]




