# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 10-sig-levels-nullmodels.py: python script to calculate the significance level of the areawise test for a given nullmodel

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
Nreals = 100
Apwaw = np.zeros((Nreals,2))

for count in range(Nreals):

    #load original data
    data0 = np.loadtxt("./data/AR1-%s.txt" % (count+1)) # or load other data here
    N = len(data0)
    datat = np.arange(1,N+1)
    dt = 1

    #set/choose parameters
    Ws = np.arange(100,301)
    Nw = len(Ws)
    dW = 1

    dim = 3

    rnm = 0 # choose network measure (0-transitivity, 1-average path length)

    cb1 = 2.5
    cb2 = 97.5


    #results for pointwise random shuffling surrogates
    nm = np.zeros((Nw,N)) 
    surrbin = np.zeros((Nw,N))

    wcount = 0
    for W in Ws:
        res = np.loadtxt('./results/AR1-%s-wrna-dim%s-W%s-dW%s.txt' % (count+1, dim, W, dW))
        surr = np.loadtxt('./surr/AR1-%s-surr-dim%s-W%s-dW%s.txt' % (count+1, dim, W, dW))
        Nres = len(res[:,0])
        nm[wcount,:-Nres] = np.nan
        nm[wcount,-Nres:] = res[:,rnm]
        surr1 = np.percentile(surr[:,rnm],cb1)
        surr2 = np.percentile(surr[:,rnm],cb2)

        for counter in range(N):
            if nm[wcount,counter] < surr1 or nm[wcount,counter] > surr2:
                surrbin[wcount,counter] = 1

        wcount = wcount + 1


    Apwaw[count,0] = np.sum(surrbin)


    #results for areawise random shuffling surrogates - factors determined by AR1 process
    mWW = -1.631 # replace with results for chosen null model
    flW = 0.3348
    mWt = 13.30
    flt = 0.5325


    Saw = awsfunc.areawise_test(N, Ws, surrbin.T, dt, mWW, flW, mWt, flt)

    Apwaw[count,1] = np.sum(Saw)


# means and standard deviations
frac = Apwaw[:,1]/Apwaw[:,0]

print np.mean(frac)
print np.std(frac)









