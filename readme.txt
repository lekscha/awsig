# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# readme.txt: description of included files 

awsfunc.py: python file containing the functions that need to be imported to run the following scripts

1-create-roessler.py: python script that creates the time series for the non-stationary Rössler system

2-psr.py: python script for phase space reconstruction of given time series using uniform time delay embedding

3-create-nullmodels.py: python script that creates a specified amount of realisations of the different null models (GWN,AR(1) and data-adaptive routine using iAAFT surrogates)

4-wavelet-decomposition.py: python script for continuous wavelet decomposition of a given time series

5a-wRNA-pwsurr.py: python script to perform windowed recurrence network analysis of a given time series including pointwise random shuffling surrogate test
5b-wSSRNA-pwsurr.py: python script to perform scale specific windowed recurrence network analysis of a given time series including pointwise random shuffling surrogate test

6a-corr-t-acf-nullmodels.py: python script to plot autocorrelation functions (time domain) for a given nullmodel
6b-corr-t-nullmodels.py: python script to calculate autocorrelation functions (time domain) for a given nullmodel
6c-corr-Ws-nullmodels.py: python script to calculate correlation functions (window width domain) for a given nullmodel

7a-fit-lines-t-all.py: python script to peform linear fit of decorrelation time in the time domain and to plot result
7b-fit-lines-Ws-all-back.py: python script to peform linear fit of decorrelation time in the window width domain and to plot result

8-binarize.py: python script to create binary matrices of (non-)significant analysis results for pointwise and areawise significance tests

9-plot-joint.py: python script to plot analysis results including contours highlighting the pointwise/areawise significant results for the different nullmodels

10-sig-levels-nullmodels.py: python script to calculate the significance level of the areawise test for a given nullmodel

 
