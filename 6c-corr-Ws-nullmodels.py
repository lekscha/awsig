# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 6c-corr-Ws-nullmodels.py: python script to calculate correlation functions (window width domain) for a given nullmodel

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc
import matplotlib.pyplot as pyplot
from matplotlib import rc 
import matplotlib.font_manager
from scipy import stats

rc('text',usetex=True)


""" --------------------------------------------------------------------------------------------------------------
# local subroutines
-------------------------------------------------------------------------------------------------------------- """
def getval2s(ac, lags):

    # get index of lags = 0
    for counter in range(len(ac)):
        if lags[counter] == 0:
            czero = counter
            break

    # calculate index where ac falls below 2/e in forward direction
    valac = ac[czero]
    siglag = czero
    while valac > 2./np.exp(1.) and siglag < len(lags)-1:

        valac = ac[siglag+1]
        siglag = siglag + 1    

    if siglag + 1 == len(lags):
        slag1 = np.nan
    else:
        slag1 = siglag + 1 - czero

    # calculate index where ac falls below 2/e in backward direction
    valac = ac[czero]
    siglag = czero
    while valac > 2./np.exp(1.) and siglag > 0:

        valac = ac[siglag-1]
        siglag = siglag - 1  

    if siglag == 0:
        slag2 = np.nan
    else:
        slag2 = siglag - 1 - czero

    return [slag1, slag2]


def correlation(data, datamat, dataind):

    M = len(datamat[:,0])
    cf = np.zeros(M)
    lags = np.zeros(M)

    for counter in range(M):
  
        pcorr = stats.pearsonr(data,datamat[counter,:])
        cf[counter] = pcorr[0]
        lags[counter] = counter - dataind         

    return [lags, cf]


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
# set parameters
dim = 3
dW = 1
rnm = 0
NG = 100

# choose window widths
Ws = np.arange(100,301)

#load taus
taus = np.loadtxt("./data/AR1-delays.txt")
taus = taus.astype(int)
maxtau = max(taus)

mtcount = 0
tau = taus[0]
while tau < maxtau:
    tau = taus[mtcount+1]
    mtcount = mtcount+1

# load transitivity data
testdat = np.loadtxt("./results/AR1-%s-wrna-dim%s-W%s-dW%s.txt" % (mtcount+1, dim, max(Ws), dW))
N = len(testdat[:,0])
lagvals1 = np.zeros((len(Ws), NG))
lagvals2 = np.zeros((len(Ws), NG))

# get transitivity as function of window width time series
figcount = 0
for count in range(NG):

    figcount = figcount + 1

    T = np.zeros((len(Ws),N))
    cftemp = np.zeros((len(Ws),len(Ws)))
    lags = np.zeros((len(Ws),len(Ws)))
    Tcount = 0

    for W in Ws:

        res = np.loadtxt("./results/AR1-%s-wrna-dim%s-W%s-dW%s.txt" % (count+1, dim, W, dW)) # or load any other data here
        T[Tcount,:] = res[-N:,rnm]

        Tcount = Tcount + 1

    pyplot.figure(figcount,figsize = (8,5))
    pyplot.plot(np.arange(-200,201),2./np.exp(1.)*np.ones(401),'--', color='black', linewidth=1.0)
    pyplot.plot(np.arange(-200,201),np.zeros(401),'-', color='black', linewidth=1.0)

    # calculate correlation functions
    for corrcount in range(len(Ws)):
        lags[corrcount,:], cftemp[corrcount,:] = correlation(T[corrcount,:], T, corrcount)
        tempvals = getval2s(cftemp[corrcount,:], lags[corrcount,:])
        lagvals1[corrcount,count] = tempvals[0]
        lagvals2[corrcount,count] = np.abs(tempvals[1])

        pyplot.plot(lags[corrcount,:],cftemp[corrcount,:], color =(1.2-Ws[corrcount]/300.,0.1,0.2), linewidth=1.0)

    pyplot.ylabel('$\mathrm{autocorrelation}$', size=21)
    pyplot.xlabel('$\mathrm{lag}$', size=21)
    pyplot.xticks(size=21)
    pyplot.yticks(size=21)
    pyplot.xlim(-200,200)
    pyplot.tight_layout()
    pyplot.savefig('./plots/acfs/acf-AR1-Ws-dim%s-NG%s-Ws300.eps' % (dim,count+1), format='eps', dpi = 200)
      

# save to file
for count in range(1,NG+1):
    Wsig = np.zeros((len(Ws),3))
    Wsig[:,0], Wsig[:,1], Wsig[:,2] = Ws, lagvals1[:,count-1], lagvals2[:,count-1]
    awsfunc.stf2d(Wsig, './eval/AR1-%s-lagvals2s-Ws-2e-dim%s-W300.txt' % (count,dim))





