# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 9-plot-joint.py: python script to plot analysis results including contours highlighting the pointwise/areawise significant results for the different nullmodels

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import matplotlib.pyplot as pyplot
from matplotlib import rc 
import matplotlib.font_manager


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
#read in the original data
data = np.loadtxt(".input/roessler.txt")
data0 = data
N = len(data0)
datat = np.loadtxt("../input/bs.txt")

#set/choose parameters
Ws = np.arange(100,301)
Nw = len(Ws)
dW = 1
tau = 5

dims = np.arange(3,4)

rnm = 0 # choose network measure (0-transitivity, 1-average path length)

cb1 = 2.5
cb2 = 97.5

rc('text',usetex=True)

dimcount = 0

for dim in dims:

    #results for pointwise random shuffling surrogates
    fig = pyplot.figure(2*dimcount+1,figsize=(12,4))

    nm = np.zeros((Nw+1,N)) 
    nm[-1,:] = np.nan
    surrbin = np.zeros((Nw+1,N))

    wcount = 0
    for W in Ws:
        res = np.loadtxt("./results/T%s-tde%s-dim%s_wrna-W%s-dW%s.txt" % (5, tau, dim, W, dW))
        Nres = len(res[:,0])
        nm[wcount,:-Nres] = np.nan
        nm[wcount,-Nres:] = res[:,rnm]

        wcount = wcount + 1

    surrtemp = np.loadtxt("./bins/ross-tde%s-dim%s-pwbinT.txt" % (tau, dim))
    surrbin[:-1,:] = surrtemp

    nmm = np.ma.masked_where(np.isnan(nm),nm)

    newWs = np.append(Ws,max(Ws)+1)
    times, tt = np.meshgrid(datat, newWs)

    pyplot.pcolormesh(times, tt, nmm,cmap='GnBu')
    pyplot.clim(vmin=np.min(np.min(nmm, axis=1), axis=0),vmax=np.max(np.max(nmm, axis=1), axis=0))
    cbar = pyplot.colorbar(aspect=5)
    cbar.ax.tick_params(labelsize=25) 
    cbar.set_label('$\mathcal{T}$', rotation=0, size = 25, labelpad=20)
    pyplot.xlim(min(datat),max(datat))
    pyplot.ylim(100,300)
    pyplot.ylabel('$\mathrm{window~width}$', size = 25)
    pyplot.xticks([0.1,0.2,0.3,0.4,0.5,0.6],size=25)
    pyplot.yticks([150,200,250],size=25)
    pyplot.axvline(x=0.0367,linewidth=1.5,color='k')
    pyplot.axvline(x=0.0931,linewidth=1.5,color='k')
    pyplot.axvline(x=0.259,linewidth=1.5,color='k')
    pyplot.axvline(x=0.367,linewidth=1.5,color='k')
    pyplot.axvline(x=0.5017,linewidth=1.5,color='k')
    pyplot.axvline(x=0.5134,linewidth=1.5,color='k')
    pyplot.contour(times, tt, surrbin, levels=[0,1], colors='black')
    pyplot.xlabel('$b$', size = 25)
    pyplot.tight_layout()
    pyplot.savefig('./plots/ross-tde%s-pwT-dim%s.png' % (tau, dim))


    #results for areawise random shuffling surrogates - Gnoise, AR1 and data adaptive 
    fig = pyplot.figure(2*dimcount+2,figsize=(12,4))

    Saw1 = np.zeros((Nw+1,N))
    Saw2 = np.zeros((Nw+1,N))
    Saw3 = np.zeros((Nw+1,N))

    Saw1temp = np.loadtxt("./bins/ross-tde%s-dim%s-awGWNbinT.txt" % (tau, dim))
    Saw2temp = np.loadtxt("./bins/ross-tde%s-dim%s-awAR1binT.txt" % (tau, dim))
    Saw3temp = np.loadtxt("./bins/ross-tde%s-dim%s-awDAbinT.txt" % (tau, dim))

    Saw1[:-1,:] = Saw1temp
    Saw2[:-1,:] = Saw2temp
    Saw3[:-1,:] = Saw3temp

    pyplot.pcolormesh(times, tt, nmm,cmap='GnBu')
    pyplot.clim(vmin=np.min(np.min(nmm, axis=1), axis=0),vmax=np.max(np.max(nmm, axis=1), axis=0))
    cbar = pyplot.colorbar(aspect=5)
    cbar.ax.tick_params(labelsize=25) 
    cbar.set_label('$\mathcal{T}$', rotation=0, size = 25, labelpad=20)
    pyplot.xlim(min(datat),max(datat))
    pyplot.ylim(100,300)
    pyplot.ylabel('$\mathrm{window~width}$', size = 25)
    pyplot.xticks([0.1,0.2,0.3,0.4,0.5,0.6],size=25)
    pyplot.yticks([150,200,250],size=25)
    pyplot.axvline(x=0.0367,linewidth=1.5,color='k')
    pyplot.axvline(x=0.0931,linewidth=1.5,color='k')
    pyplot.axvline(x=0.259,linewidth=1.5,color='k')
    pyplot.axvline(x=0.367,linewidth=1.5,color='k')
    pyplot.axvline(x=0.5017,linewidth=1.5,color='k')
    pyplot.axvline(x=0.5134,linewidth=1.5,color='k')
    pyplot.contour(times, tt, Saw1, levels=[0,1], colors='darkblue')
    pyplot.contour(times, tt, Saw2, levels=[0,1], colors='darkgreen')
    pyplot.contour(times, tt, Saw3, levels=[0,1], colors='darkred')
    pyplot.xlabel('$b$', size = 25)
    pyplot.tight_layout()
    pyplot.savefig('./plots/ross-tde%s-awTjoint-dim%s.png' % (tau, dim))


    dimcount = dimcount+1




