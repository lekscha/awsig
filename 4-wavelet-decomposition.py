# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 4-wavelet-decomposition.py: python script for continuous wavelet decomposition of a given time series

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc
import pywt


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
mcnt = 1

# choose embedding parameters
dim = 3
taus = np.loadtxt("./data/AR1-delays.txt") # choose correct file depending on time series here
taus = taus.astype(int)
tau = taus[mcnt-1]

# read in the embedded data
data = np.loadtxt("./data/AR1-%s-tde%s-dim%s.txt" % (mcnt, tau, dim)) # or load any other embedded data set here
time = data[:,0]
N = len(time)
dt = 1

# set wavelet params
scales = np.arange(0.4,16.2,0.2)
wavelet = 'cmor2.0-1.0'
frequencies = pywt.scale2frequency(wavelet, scales) / dt
Nscales = len(scales)
sf = np.zeros((Nscales,2))
sf[:,0], sf[:,1] = scales, frequencies

# save scales and frequencies to file
awsfunc.stf2d(sf, './data/AR1-%s-wav-scales-frequs.txt' % mcnt)

# apply continuous wavelet transform to decompose signal (seperately for each dimension)
for dcount in range(dim):
    coef, freqs = pywt.cwt(data[:,dcount+1],scales,wavelet)

    wcoefr = np.zeros((N,Nscales+1))
    wcoefr[:,0] = time
    for count in range(Nscales):
        wcoefr[:,count+1] = np.real(coef[count,:])

    wcoefi = np.zeros((N,Nscales+1))
    wcoefi[:,0] = time
    for count in range(Nscales):
        wcoefi[:,count+1] = np.imag(coef[count,:])

    # save to file
    awsfunc.stf2d(wcoefr, './data/AR1-%s-tde%s-dim%s-part%s-wav-cmor2-1-real.txt' % (mcnt, tau, dim, dcount+1))
    awsfunc.stf2d(wcoefi, './data/AR1-%s-tde%s-dim%s-part%s-wav-cmor2-1-imag.txt' % (mcnt, tau, dim, dcount+1))






