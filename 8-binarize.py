# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 8-binarize.py: python script to create binary matrices of (non-)significant analysis results for pointwise and areawise significance tests

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
#read in the original data
data = np.loadtxt("./input/roessler.txt")
data0 = data
N = len(data0)
datat = np.arange(1,N+1)
dt = 1

#set/choose parameters
Ws = np.arange(100,301)
Nw = len(Ws)
dW = 1
tau = 5 # embedding delay of original data

dim = 3

rnm = 0 # choose network measure (0-transitivity, 1-average path length)

cb1 = 2.5
cb2 = 97.5


#results for pointwise random shuffling surrogates
nm = np.zeros((Nw,N)) 
surrbin = np.zeros((Nw,N))

wcount = 0
for W in Ws:
    res = np.loadtxt("./results/T%s-tde%s-dim%s_wrna-W%s-dW%s.txt" % (5, tau, dim, W, dW))
    surr = np.loadtxt("./surr/T%s-tde%s-dim%s_surr-W%s-dW%s.txt" % (5, tau, dim, W, dW))
    Nres = len(res[:,0])
    nm[wcount,:-Nres] = np.nan
    nm[wcount,-Nres:] = res[:,rnm]
    surr1 = np.percentile(surr[:,rnm],cb1)
    surr2 = np.percentile(surr[:,rnm],cb2)

    for counter in range(N):
        if nm[wcount,counter] < surr1 or nm[wcount,counter] > surr2:
            surrbin[wcount,counter] = 1

    wcount = wcount + 1

awsfunc.stf2d(surrbin, './bins/ross-tde%s-dim%s-pwbinT.txt' % (tau, dim))

print np.sum(surrbin)


#results for areawise random shuffling surrogates - factors determined by Gaussian noise 
mWW = -5.081
flW = 0.2527
mWt = -2.364
flt = 0.3813

Saw = awsfunc.areawise_test(N, Ws, surrbin.T, dt, mWW, flW, mWt, flt)

awsfunc.stf2d(Saw.T, './bins/ross-tde%s-dim%s-awGWNbinT.txt' % (tau, dim))

print np.sum(Saw)



#results for areawise random shuffling surrogates - factors determined by AR(1) process 
mWW = -1.631
flW = 0.3348
mWt = 13.30
flt = 0.5325

Saw = awsfunc.areawise_test(N, Ws, surrbin.T, dt, mWW, flW, mWt, flt)

awsfunc.stf2d(Saw.T, './bins/ross-tde%s-dim%s-awAR1binT.txt' % (tau, dim))

print np.sum(Saw)



#results for areawise random shuffling surrogates - factors determined data-adaptively
mWW = -32.54
flW = 0.4102
mWt = -12.74
flt = 0.5276

Saw = awsfunc.areawise_test(N, Ws, surrbin.T, dt, mWW, flW, mWt, flt)

awsfunc.stf2d(Saw.T, './bins/ross-tde%s-dim%s-awDAbinT.txt' % (tau, dim))

print np.sum(Saw)








