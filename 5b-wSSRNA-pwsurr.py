# Project: Areawise significance tests for recurrence network analysis
# Jaqueline Lekscha and Reik V. Donner
# code for paper submitted to RSPA
# written by Jaqueline Lekscha 
# Potsdam, 11 March 2019

# 5b-wSSRNA-pwsurr.py: python script to perform scale specific windowed recurrence network analysis of a given time series including pointwise random shuffling surrogate test

""" --------------------------------------------------------------------------------------------------------------
# import required modules
-------------------------------------------------------------------------------------------------------------- """
import numpy as np
import awsfunc


""" --------------------------------------------------------------------------------------------------------------
# main routine
-------------------------------------------------------------------------------------------------------------- """
mcnt = 1

#set parameters
dim = 3
taus = np.loadtxt("./data/AR1-delays.txt") # or load any other data set here
taus = taus.astype(int)
tau = taus[mcnt-1]
dW = 1
RR = 0.05
Nsurr = 1000

#read in scales and orig embedded data
sf = np.loadtxt('./data/AR1-%s-wav-scales-frequs.txt' % mcnt)
scales = sf[:,0]
Nscales = len(scales)
data = np.loadtxt("./data/AR1-%s-tde%s-dim%s.txt" % (mcnt, tau, dim)) # just to get length of data set
N = len(data[:,1])

#read in data
data_emb = np.zeros((N,dim,Nscales))
for dcount in range(dim):
    temp = np.loadtxt('./data/AR1-%s-tde%s-dim%s-part%s-wav-cmor2-1-real.txt' % (mcnt, tau, dim, dcount+1)) # or load any other wavelet-filtered data set here
    data_emb[:,dcount,:] = temp[:,1:]


for scount in range(Nscales):

    W = max(100,int(16*scales[scount]+1))

    cutoff = int(8*scales[scount])

    #windowed recurrence network analysis
    T, L = awsfunc.wrna(data_emb[cutoff:-cutoff,:,scount], W, dW, RR)
    Nw = len(T)
    TL = np.zeros((Nw,2))
    TL[:,0], TL[:,1] = T, L
    awsfunc.stf2d(TL, './results/AR1-%s-tde%s-dim%s_wrna-W%s-dW%s_wav%s.txt' % (mcnt, tau, dim, W, dW, scount+1))

    #surrogates for wrna
    T, L = awsfunc.wrna_surr(data_emb[cutoff:-cutoff,:,scount], W, RR, Nsurr)
    TL = np.zeros((Nsurr,2))
    TL[:,0], TL[:,1] = T, L
    awsfunc.stf2d(TL, './surr/AR1-%s-tde%s-dim%s_surr-W%s-dW%s_wav%s.txt' % (mcnt, tau, dim, W, dW, scount+1))



